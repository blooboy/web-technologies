class Person{
	constructor(name){
		this.name = name
	}
}

class Student extends Person{
	constructor(name, faculty, year){
		super(name)
		this.faculty = faculty
		this.year = year
		this.grades = []
	}
	
	addGrade(grade){
		this.grades.push(grade)
	}
	
	isPassing(){
		let x = this.grades.map(y => y.value)
		let s=0
		for(let i=0;i<x.length;i++){
			s=s+x[i]
		}
		return s/x.length>=6
	}
	
	getPassingGrades(){
		let q= this.grades.filter( x => x.value >=5)
		let w= q.map( q => q.value)
		return w
	}
	
	getFailedSubjects(){
		let w = this.grades.filter(x => x.value <=5)
		let q=w.map( x => x.subject)
		return q
	}
}

class Grade{
	constructor(subject, value){
		this.subject = subject
		this.value = value
	}
	isPassingGrade(){
		return this.value >=5
	}
}

module.exports.Student = Student
module.exports.Grade = Grade