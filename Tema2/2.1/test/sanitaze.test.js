let assert = require('assert')

describe('sanitize', function(){
	let sanitize = require('../app').sanitize
	let dictionary = require('../app').dictionary
	it('replaces the words javascript and java', function(){
		assert.equal(sanitize('javascript and java are different programming languages', dictionary), 'j********t and j**a are different programming languages')
	})
	it('replaces the word python', function(){
		assert.equal(sanitize('python was created in 1991', dictionary), 'p****n was created in 1991')
	})
	it('replaces the words python', function(){
		assert.equal(sanitize('the most popular enterprise language is java, while the most popular machine learning language is python.', dictionary), 'the most popular enterprise language is j**a, while the most popular machine learning language is p****n.')
	})
})