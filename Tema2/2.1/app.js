const dictionary = ['javascript', 'java', 'python']

/*
functia sanitize primeste ca parametru un text si un dictionar
cuvintele din dictionar sunt inlocuite in text cu prima litera urmata de o serie de asteriscuri (egala cu numarul de litere inlocuite) urmata de ultima litera
e.g. daca 'decembrie' exista in dictionar, va fi inlocuit cu 'd*******e'
*/


function sanitize(text, dictionary){
    
let text1=text.replace(","," ,");
let words=text1.replace("."," .").split(" ");


let result=[]
for(let word of words)
{
  let neword=word;
 if(dictionary.indexOf(word)>-1)
  {
         for(let i=1;i<word.length-1;i++)
     {
          neword=neword.replace(word[i],"*")
     }
     
  }
  result.push(neword)
}


return result.join(" ").replace(" ,",",").replace(" .",".")

}


module.exports.dictionary = dictionary
module.exports.sanitize = sanitize