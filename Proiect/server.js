'use strict';
const express = require('express');
const bodyParser = require('body-parser');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const sequelize = new Sequelize('proiect','root','',{
	dialect : 'mysql',
	define : { 
		timestamps : false
	}
});

const User = sequelize.define('user', {
	firstName : {
	    type: Sequelize.STRING,
	    allowNull: false,
	    validate: {
	        len: [3,20]
	    }
	},
	lastName : {
	    type: Sequelize.STRING,
	    allowNull: false,
	    validate: {
	        len: [3,20]
	    }
	},
	username : {
	    type: Sequelize.STRING,
	    allowNull : false,
	    validate : {
	        len: [3,20]
	    },
	    unique:{
	        args: true,
	        msg: 'Username already in use!'
	    }
	},
	password :{
	    type : Sequelize.STRING,
	    allowNull: false,
	    validate : {
	        len: [3,20]
	    }
	},
	email: {
	    type: Sequelize.STRING,
	    allowNull: false,
	    validate: {
            isEmail:true
        },
        unique: {
            args: true,
            msg: 'Email address already in use!'
        }
	}
	
});

const Project = sequelize.define('project',{
    projectName : {
        type: Sequelize.STRING,
	    allowNull: false,
	    validate: {
	        len: [3,20]
	    }
    },
    startDate : {
        type: Sequelize.DATE,
    },
    deadline : {
        type: Sequelize.DATE,
        allowNull: false,
    },
    description : {
        type: Sequelize.STRING,
    }
});

const Event = sequelize.define('event',{
    date : {
        type : Sequelize.DATE,
        allowNull : false,
    },
    description : {
        type: Sequelize.STRING,
        allowNull : false,
        validate :{
            len: [10,100]
        }
    }
});

User.hasMany(Project);
Project.hasMany(Event);

const app = express();
app.use(bodyParser.json());

app.get('/create', async (req, res) => {
	try{
		await sequelize.sync({force : true})
		res.status(201).json({message : 'created'});
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'});
	}
});

app.get('/projects', async (req, res) => {
	try{
		let pageSize = 10;
		let params = {
			where: {},
			order: [['deadline','DESC']]
		};
	    let projects = await Project.findAll(params);
		res.status(200).json(projects);
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'});
	}
});

app.get('/users',async (req, res) => {
	try{
		let pageSize = 10;
		let params = {
			where: {}
		};
	    let users = await User.findAll(params);
		res.status(200).json(users);
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'});
	}
});

app.post('/projects', async (req, res) => {
	try{
		if (req.query.bulk && req.query.bulk == 'on'){
			await Project.bulkCreate(req.body)
			res.status(201).json({message : 'created'});
		}
		else{
			await Project.create(req.body)
			res.status(201).json({message : 'created'});
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'});
	}
});

app.post('/users',async (req, res) => {
	try{
		if (req.query.bulk && req.query.bulk == 'on'){
			await User.bulkCreate(req.body)
			res.status(201).json({message : 'created'});
		}
		else{
			await User.create(req.body)
			res.status(201).json({message : 'created'});
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'});
	}
});

app.post('/events',async (req, res) => {
	try{
		if (req.query.bulk && req.query.bulk == 'on'){
			await Event.bulkCreate(req.body)
			res.status(201).json({message : 'created'});
		}
		else{
			await Event.create(req.body)
			res.status(201).json({message : 'created'});
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'});
	}
});

app.delete('/projects/:id', async (req, res) => {
	try{
		let project = await Project.findById(req.params.id);
		if (project){
			await project.destroy()
			res.status(202).json({message : 'accepted'});
		}
		else{
			res.status(404).json({message : 'not found'});
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'});
	}
});

app.delete('/users/:id', async (req, res) => {
	try{
		let user = await User.findById(req.params.id);
		if (user){
			await user.destroy()
			res.status(202).json({message : 'accepted'});
		}
		else{
			res.status(404).json({message : 'not found'});
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'});
	}
});

app.delete('/events/:id', async (req, res) => {
	try{
		let event = await Event.findById(req.params.id);
		if (event){
			await event.destroy()
			res.status(202).json({message : 'accepted'});
		}
		else{
			res.status(404).json({message : 'not found'});
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'});
	}
});

app.get('/projects/:id' , async (req, res) => {
	try{
		let project = await Project.findById(req.params.id);
		if (project){
			res.status(200).json(prompt());
		}
		else{
			res.status(404).json({message : 'not found'});
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'});
	}
});

app.get('/projects/:id/events',async(req,res) => {
   	try {
		let project = await Project.findById(req.params.id, {include : ['events']});
		if (project){
			res.status(200).json(project.events);
		}
		else{
			res.status(404).json({message : 'not found'});
		}
	} catch (e) {
		console.warn(e.stack)
		res.status(500).json({message : 'server error'});
	}
});

app.post('/users/:id/projects',async (req, res) => {
	try {
		let user = await User.findById(req.params.id);
		if (user){
		    let project = req.body;
			project.userId = user.id;
			await Project.create(project)
			res.status(201).json({message : 'created'});
		}
		else{
			res.status(404).json({message : 'not found'});
		}
	} catch (e) {
		console.warn(e.stack)
		res.status(500).json({message : 'server error'});
	}	
	
});

app.listen(8080);